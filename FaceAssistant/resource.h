//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FaceAssistant.rc
//
#define IDD_FACEASSISTANT_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDC_BUTTON_OPEN                 1000
#define IDC_LIST_FACE                   1002
#define IDC_LIST_NUM                    1003
#define IDC_STATIC_PIC                  1004
#define IDC_SCROLLBAR1                  1005
#define IDC_SCROLLBAR2                  1006
#define IDC_EDIT1                       1007
#define IDC_BUTTON1                     1008
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
