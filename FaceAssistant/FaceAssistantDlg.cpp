
// FaceAssistantDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FaceAssistant.h"
#include "FaceAssistantDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFaceAssistantDlg 对话框




CFaceAssistantDlg::CFaceAssistantDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFaceAssistantDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_image=NULL;
	m_dspbuf=NULL;
	addFaceFlag=false;
	m_hpos=0;
	m_vpos=0;
}

void CFaceAssistantDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SCROLLBAR1, m_SBar);
	DDX_Control(pDX, IDC_SCROLLBAR2, m_SvBar);
}

BEGIN_MESSAGE_MAP(CFaceAssistantDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_OPEN, &CFaceAssistantDlg::OnOpenFile)
	ON_LBN_SELCHANGE(IDC_LIST_FACE, &CFaceAssistantDlg::OnShowSigleFace)
	ON_LBN_SELCHANGE(IDC_LIST_NUM, &CFaceAssistantDlg::OnShowSingleFrame)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
//	ON_COMMAND(ID_32771, &CFaceAssistantDlg::OnShowAllFaces)
	ON_COMMAND(ID_32772, &CFaceAssistantDlg::OnDeleteSelectedFace)
	ON_WM_RBUTTONUP()
	ON_WM_CLOSE()
	ON_COMMAND(ID_32773, &CFaceAssistantDlg::OnDeleteAllFaces)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_BN_CLICKED(IDC_BUTTON1, &CFaceAssistantDlg::changeName)
END_MESSAGE_MAP()


// CFaceAssistantDlg 消息处理程序

BOOL CFaceAssistantDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	MyInit();
	

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CFaceAssistantDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}

	if(m_dspbuf )
	{
		CPaintDC dc(GetDlgItem(IDC_STATIC_PIC));

		CDC dcMem;
		CBitmap bmpMem;
		dcMem.CreateCompatibleDC(&dc);
		bmpMem.CreateCompatibleBitmap(&dc, m_dspbuf->width, m_dspbuf->height);
		dcMem.SelectObject(&bmpMem);

		int linebyte;
		int w = m_dspbuf->width;
		int h = m_dspbuf->height;
		linebyte = m_dspbuf->widthStep;

		bmpMem.SetBitmapBits(linebyte * h, m_dspbuf->imageData);

		dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);

		bmpMem.DeleteObject();
		dcMem.DeleteDC(); 
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CFaceAssistantDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CFaceAssistantDlg::OnOpenFile()
{
	SaveFaceInfoToTxt();
	SaveRecognitionInfo();

	CFileDialog fdlg(TRUE, NULL,NULL, OFN_NOCHANGEDIR);

	if( fdlg.DoModal() == IDOK )
	{
		m_currentFilePath = fdlg.GetPathName();
		GetFilenameFromAbsolutePath(m_currentFilePath,m_currentFileName);

		//判断 是图片还是视频
		m_fileType=IsPictureOrVideo(m_currentFilePath);

		//获取 坐标
		CString txtPath=CurrentPath+"\\detection.txt";
		GetAnalysisTime(m_AnalysisTime,txtPath);
		GetFaceInfoFromTxt(m_fileType-1);//获取图片或者视频第一帧人脸信息
		
		// 显示 当前图片所有人脸
		if (m_fileType==1)//图片
		{
			myLoadPictureBy4channels(m_currentFilePath,m_image);
			((CListBox* )GetDlgItem(IDC_LIST_NUM))->ResetContent();
		}
		else if (m_fileType==2)
		{
			//获取识别信息
			GetRecognitionInfo();//获取 识别信息

			GetPictureFromVideoByIndex(m_currentFilePath,m_image,0);

			int frameCounts=GetVideoFraneCounts(m_currentFilePath);

			((CListBox* )GetDlgItem(IDC_LIST_NUM))->ResetContent();
			for (int i=0;i<frameCounts;i++)
			{	
				CString tmp;
				CString txt;
				tmp.Format("%05d",(i+1));
				txt="帧 "+tmp;

				((CListBox* )GetDlgItem(IDC_LIST_NUM))->InsertString(i,txt);
			}

			((CListBox* )GetDlgItem(IDC_LIST_NUM))->SetCurSel(0);
			CurrentFrameNum=1;
		}

		MySetWindow();
		InitScroll();
		GetImageDisplayData(m_dspbuf, m_image,  m_hpos,m_vpos);
		DrawAllFace( m_dspbuf);
		Invalidate(FALSE);

	}
}
void CFaceAssistantDlg::ReadAnalysisTime(vector<CString> &paths,CString txtPath)
{
	CString Path;//文件路径
	char *filePath;
	FILE *File=NULL;
	Path=txtPath;

	ClearCStringVector(paths);
	filePath=Path.GetBuffer(Path.GetLength());
	CString tmp;

	if (PathFileExists(Path))
	{
		fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			while(!feof(File))//(feof返回1，说明到文件结尾)
			{
				//路径
				ReadCStringR(tmp,File);
				if ("Cognivisual Technologies.Inc"==tmp)
				{
					break;
				}

				paths.push_back(tmp);
				//日期
				ReadCStringR(tmp,File);
				paths.push_back(tmp);

			}	

		}
		fclose(File);
		File=NULL;
	}
}
void CFaceAssistantDlg::GetAnalysisTime(CString &AnalysisTime,CString txtPath)
{
	AnalysisTime="#####";

	vector<CString> paths;//存储路径以及编号
	ReadAnalysisTime(paths,txtPath);

	int n=(int)paths.size();
	int i=0;

	if (n<2)
	{
		return;
	}

	bool f=false;
	for (i=0;i<n/2;i++)
	{	
		if (paths[2*i]==m_currentFilePath)
		{
			f=true;
			AnalysisTime=paths[2*i+1];
			break;
		}		
	}

}
void CFaceAssistantDlg::GetFaceInfoFromTxt(int TxtIndex)
{
	CString Path;//文件路径
	char *filePath;
	FILE *File=NULL;

	CString tmp;
	tmp.Format("%09d",TxtIndex);
	Path=CurrentPath+"\\Record\\"+m_AnalysisTime+"\\"+tmp+".txt";
	filePath=Path.GetBuffer(Path.GetLength());


	int xmin;
	if (!m_faces.empty())
	{
		m_faces.clear();
		vector<int>().swap(m_faces);
	}

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n;
		fread(&n,sizeof(int),1,File);

		for (int i=0;i<n*4;i++)
		{	
			fread(&xmin,sizeof(int),1,File);	
			m_faces.push_back(xmin);
		}
		fclose(File);
	}
}
void CFaceAssistantDlg::WriteFaceInfoToTxt(int TxtIndex)
{
	int i;
	char*filePath;

	CString tmp;
	tmp.Format("%09d",TxtIndex);
	CString Path=CurrentPath+"\\Record\\"+m_AnalysisTime+"\\"+tmp+".txt";
	filePath=Path.GetBuffer(Path.GetLength());

	FILE *File=NULL;
	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n=m_faces.size()/4;
		fwrite(&n,sizeof(int),1,File);

		for (i=0;i<(n*4);i++)
		{	
			fwrite(&m_faces[i],sizeof(int),1,File);	
		}
		fclose(File);
	}

	File=NULL;
}
void CFaceAssistantDlg::MyInit()//初始化设置
{
	GetCurrentPath(CurrentPath);

	int iWidth = GetSystemMetrics(SM_CXSCREEN); 
	int iHeight = GetSystemMetrics(SM_CYSCREEN); 
	int x,y;

	x=iWidth/2-480;
	y=iHeight/2-270;

	if (iWidth<960||iHeight<540)
	{
		ShowWindow(SW_MAXIMIZE);
	}
	else
	{
		this->SetWindowPos(0,x,y,960,540,SWP_SHOWWINDOW);
	}

	CRect rect;
	GetDlgItem(IDC_BUTTON_OPEN)->GetWindowRect(&rect);
	GetDlgItem(IDC_BUTTON_OPEN)->SetWindowPos(0,10,5,rect.Width(),rect.Height(),SWP_SHOWWINDOW);
	GetDlgItem(IDC_EDIT1)->SetWindowPos(0,100,5,200,20,SWP_SHOWWINDOW);
	GetDlgItem(IDC_BUTTON1)->SetWindowPos(0,300,5,rect.Width(),rect.Height(),SWP_SHOWWINDOW);
	
}
void CFaceAssistantDlg::MySetWindow()
{
	int iWidth = GetSystemMetrics(SM_CXSCREEN); 
	int iHeight = GetSystemMetrics(SM_CYSCREEN); 
	int x,y,w,h,mh,mw;



	x=iWidth/2-480;
	y=iHeight/2-270;

	this->SetWindowPos(0,x,y,960,540,SWP_SHOWWINDOW);





	mw=m_image->width;
	mh=m_image->height;
	
	w=8+mw+90;//  图片显示区域+ 列表
	h=30+30+mh;//上边框+图片显示高度+按钮区域


	x=(iWidth-w)/2;
	y=(iHeight-h)/2;

	if (iWidth<w||iHeight<(h+40))
	{
		ShowWindow(SW_MAXIMIZE);
	}
	else
	{
		this->SetWindowPos(0,x,y,w,h,SWP_SHOWWINDOW);
	}

	CRect rect;
	GetDlgItem(IDC_BUTTON_OPEN)->GetWindowRect(&rect);
	GetDlgItem(IDC_BUTTON_OPEN)->SetWindowPos(0,10,5,rect.Width(),rect.Height(),SWP_SHOWWINDOW);

	GetDlgItem(IDC_EDIT1)->SetWindowPos(0,100,5,200,20,SWP_SHOWWINDOW);
	GetDlgItem(IDC_BUTTON1)->SetWindowPos(0,300,5,rect.Width(),rect.Height(),SWP_SHOWWINDOW);

	CRect clrect;
	CvSize dst_cvsize;


	GetClientRect(&clrect);

	dst_cvsize.width=clrect.Width()-90;
	dst_cvsize.height=clrect.Height()-30;

	int l;
	if (mw>dst_cvsize.width)//水平进度条
	{
		l=15;	
		GetDlgItem(IDC_SCROLLBAR1)->MoveWindow(0,clrect.Height()-15,dst_cvsize.width,15,TRUE);//水平滚动
		GetDlgItem(IDC_SCROLLBAR1)->ShowWindow(SW_SHOW);
	}
	else
	{
		l=0;
	}
	int h1;//垂直进度条
	if (mh>dst_cvsize.height)//垂直进度条
	{
		dst_cvsize.height-=15;

		GetDlgItem(IDC_SCROLLBAR2)->MoveWindow(dst_cvsize.width,0,15,clrect.Height(),TRUE);//垂直滚动
		GetDlgItem(IDC_SCROLLBAR2)->ShowWindow(SW_SHOW);
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	m_dspbuf=NULL;
	cvReleaseImage(&m_dspbuf);
	m_dspbuf=cvCreateImage(dst_cvsize,8,4);
	GetDlgItem(IDC_STATIC_PIC)->MoveWindow(0,30,dst_cvsize.width,dst_cvsize.height);
	GetDlgItem(IDC_STATIC_PIC)->ShowWindow(SW_SHOWNA);

	
	GetDlgItem(IDC_LIST_FACE)->MoveWindow(dst_cvsize.width+l,0,90-l,dst_cvsize.height/2-5);
	GetDlgItem(IDC_LIST_FACE)->ShowWindow(SW_SHOWNA);

	GetDlgItem(IDC_LIST_NUM)->MoveWindow(dst_cvsize.width+l,dst_cvsize.height/2,90-l,dst_cvsize.height/2-5);
	GetDlgItem(IDC_LIST_NUM)->ShowWindow(SW_SHOWNA);
}

void CFaceAssistantDlg::DrawAllFace( IplImage* src,bool f)
{
	int n=(int)m_faces.size()/4;
	if (f==true)
	{
		((CListBox* )GetDlgItem(IDC_LIST_FACE))->ResetContent();
	}
	
	for (int i=0;i<n;i++)
	{	
		int l=m_faces[4*i]-m_hpos;
		int r=m_faces[4*i+2]-m_hpos;
		int t=m_faces[4*i+1]-m_vpos;
		int b=m_faces[4*i+3]-m_vpos;

		myDrawrectangular( src,l,t,r,b,255,255,255);

		int framenum=((CListBox* )GetDlgItem(IDC_LIST_NUM))->GetCurSel();
		
		CString tmp;
		CString txt;
		tmp.Format("%02d",(i+1));
		if (i>=allface[framenum].name.size())
		{
			allface[framenum].name.push_back("");
		}
		if (allface[framenum].name[i]!="")
		{
			txt=allface[framenum].name[i];
			tmp=txt;
		}
		else
		{
			txt="人脸"+tmp;
		}
		

		if (f==true)
		{
			((CListBox* )GetDlgItem(IDC_LIST_FACE))->InsertString(i,txt);
		}

		CvFont font;//以下用文字标识说明
		double hScale=1, vScale=1;
		int lineWidth=1;
		cvInitFont(&font,CV_FONT_HERSHEY_PLAIN  |CV_FONT_ITALIC,hScale,vScale,1,lineWidth,8);
		char *buf; 
		buf = (LPSTR)(LPCTSTR)tmp;
		int point_x=m_faces[4*i]-m_hpos;
		int point_y=m_faces[4*i+1]-m_vpos;
		cvPutText(m_dspbuf, buf, cvPoint(point_x, point_y - 5), &font, CV_RGB(0,255,255));
		
	}
	
}

void CFaceAssistantDlg::OnShowSigleFace()
{
	// TODO: 在此添加控件通知处理程序代码
	int i=((CListBox* )GetDlgItem(IDC_LIST_FACE))->GetCurSel();

	if (i>=0)
	{
		int l=m_faces[4*i]-m_hpos;
		int r=m_faces[4*i+2]-m_hpos;
		int t=m_faces[4*i+1]-m_vpos;
		int b=m_faces[4*i+3]-m_vpos;

		if (t<=0)
		{
			m_vpos=(m_faces[4*i+1]-m_dspbuf->height)<0?0:(m_faces[4*i+1]-m_dspbuf->height);

			int position=m_vpos*100/(m_image->height-m_dspbuf->height);
			m_SvBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
		}
		if (b>=m_dspbuf->height)
		{
			m_vpos=m_faces[4*i+3]-m_dspbuf->height;

			int position=m_vpos*100/(m_image->height-m_dspbuf->height);
			m_SvBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
		}

		if (l<=0)
		{
			m_hpos=(m_faces[4*i+1]-m_dspbuf->width)<0?0:(m_faces[4*i+1]-m_dspbuf->width);

			int position=m_hpos*100/(m_image->width-m_dspbuf->width);
			m_SBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
		}

		if (r>=m_dspbuf->width)
		{
			m_hpos=m_faces[4*i+2]-m_dspbuf->width;

			int position=m_hpos*100/(m_image->width-m_dspbuf->width);
			m_SBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
		}
		GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);

	}

	ShowSigleFace();

}


void CFaceAssistantDlg::OnShowSingleFrame()
{
	if (m_fileType!=2)
	{
		return;
	}
	SaveFaceInfoToTxt();

	// TODO: 在此添加控件通知处理程序代码
	int i=((CListBox* )GetDlgItem(IDC_LIST_NUM))->GetCurSel();

	if (i>=0)
	{
		GetFaceInfoFromTxt(i+1);//视频帧记录序号 从1开始
		GetPictureFromVideoByIndex(m_currentFilePath,m_image,i);
//		MySetWindow();
		InitScroll();
		GetImageDisplayData(m_dspbuf, m_image,  m_hpos,m_vpos);
		DrawAllFace( m_dspbuf);
		Invalidate(FALSE);
		CurrentFrameNum=i+1;//视频帧序号比listbox 多1
	}
}


void CFaceAssistantDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	int i=((CListBox* )GetDlgItem(IDC_LIST_FACE))->GetCurSel();

	if (i>=0)
	{
		if (nFlags==MK_RBUTTON)
		{
			int x=point.x-m_point.x;
			int y=point.y-m_point.y;

			m_faces[4*i]+=x;
			m_faces[4*i+1]+=y,m_faces[4*i+2]+=x,m_faces[4*i+3]+=y;

			GetImageDisplayData(m_dspbuf, m_image,  m_hpos,m_vpos);

			int l=m_faces[4*i]-m_hpos;
			int r=m_faces[4*i+2]-m_hpos;
			int t=m_faces[4*i+1]-m_vpos;
			int b=m_faces[4*i+3]-m_vpos;

			myDrawrectangular( m_dspbuf,l,t,r,b,0,255,0);
//			myDrawrectangular( m_dspbuf,m_faces[4*i],m_faces[4*i+1],m_faces[4*i+2],m_faces[4*i+3],0,255,0);

			Invalidate(FALSE);


			int framenum=((CListBox* )GetDlgItem(IDC_LIST_NUM))->GetCurSel();


			allface[framenum].faces[4*i]=m_faces[4*i];
			allface[framenum].faces[4*i+1]=m_faces[4*i+1];
			allface[framenum].faces[4*i+2]=m_faces[4*i+2];
			allface[framenum].faces[4*i+3]=m_faces[4*i+3];
		}

		
	}

	if (nFlags==MK_LBUTTON&&point.y>30)
	{
		if (addFaceFlag==false)
		{
			leftUpPoint=point;
			addFaceFlag=true;
		}


		int x,y;
		x=point.x-leftUpPoint.x;
		y=point.y-leftUpPoint.y;

		int d=abs(x)>abs(y)?x:y;

		if (abs(d)>0)
		{
			GetImageDisplayData(m_dspbuf, m_image,  m_hpos,m_vpos);

// 			int l=m_faces[4*i]-m_hpos;
// 			int r=m_faces[4*i+2]-m_hpos;
// 			int t=m_faces[4*i+1]-m_vpos;
// 			int b=m_faces[4*i+3]-m_vpos;
// 
// 			myDrawrectangular( m_dspbuf,l,t,r,b,0,255,0);

			myDrawrectangular( m_dspbuf,leftUpPoint.x,leftUpPoint.y-30,leftUpPoint.x+d,leftUpPoint.y-30+d,0,255,0);

			Invalidate(FALSE);
		}
		
	}


	m_point=point;

	CDialogEx::OnMouseMove(nFlags, point);
}


void CFaceAssistantDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	if (addFaceFlag==true)
	{
		int x,y;
		x=point.x-leftUpPoint.x;
		y=point.y-leftUpPoint.y;

		int d=abs(x)>abs(y)?x:y;

		if (abs(d)>0&&d>0)
		{
			int l=leftUpPoint.x+m_hpos;
			int r=leftUpPoint.x+d+m_hpos;
			int t=leftUpPoint.y-30+m_vpos;
			int b=leftUpPoint.y+d-30+m_vpos;

			m_faces.push_back(l);
			m_faces.push_back(t);
			m_faces.push_back(r);
			m_faces.push_back(b);

			int k=(int)(m_faces.size()/4);
			CString tmp;
			CString txt;
			tmp.Format("%02d",k);
			txt="人脸"+tmp;

			((CListBox* )GetDlgItem(IDC_LIST_FACE))->InsertString(k-1,txt);
			((CListBox* )GetDlgItem(IDC_LIST_FACE))->SetCurSel(k-1);

			int framenum=((CListBox* )GetDlgItem(IDC_LIST_NUM))->GetCurSel();

			
			allface[framenum].faces.push_back(l);
			allface[framenum].faces.push_back(t);
			allface[framenum].faces.push_back(r);
			allface[framenum].faces.push_back(b);
			
		}
		
		OnShowSigleFace();
		addFaceFlag=false;
	}
	
	

	CDialogEx::OnLButtonUp(nFlags, point);
}
BOOL CFaceAssistantDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(WM_RBUTTONDOWN==pMsg->message)
		if((pMsg->hwnd==((CListBox* )GetDlgItem(IDC_LIST_FACE))->m_hWnd)&&(m_image))
		{
			DWORD dwPos=GetMessagePos();
			CPoint point(LOWORD(dwPos),HIWORD(dwPos));
			CMenu menu;
			VERIFY(menu.LoadMenu(IDR_MENU1));
			CMenu *popup=menu.GetSubMenu(0);
			ASSERT(popup!=NULL);
			popup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,point.x,point.y,this);

		}

		if(pMsg->message==WM_KEYDOWN  )
		{
			if (pMsg->wParam=='S')//保存
			{
				SaveFaceInfoToTxt();
			}
			else if(pMsg->wParam=='O')//打开文件
			{
				OnOpenFile();
			}
			else if (pMsg->wParam=='D')//删除一个人脸
			{
				OnDeleteSelectedFace();
			}
			else if (pMsg->wParam=='R')//删除所有脸
			{
				OnDeleteAllFaces();
			}
			else if (pMsg->wParam=='F')//下一帧
			{
				if (CurrentFrameNum>0)
				{
					((CListBox* )GetDlgItem(IDC_LIST_NUM))->SetCurSel(CurrentFrameNum);
					OnShowSingleFrame();
				}
				
			}
			else if (pMsg->wParam=='B')//前一帧
			{
				if (CurrentFrameNum>1)
				{
					((CListBox* )GetDlgItem(IDC_LIST_NUM))->SetCurSel(CurrentFrameNum-2);
					OnShowSingleFrame();
				}
			}



		}
		return CDialog::PreTranslateMessage(pMsg);
}


void CFaceAssistantDlg::OnShowAllFaces()
{
	DrawAllFace( m_dspbuf);
	Invalidate(FALSE);
}


void CFaceAssistantDlg::OnDeleteSelectedFace()
{
	int framenum=((CListBox* )GetDlgItem(IDC_LIST_NUM))->GetCurSel();

	int i=((CListBox* )GetDlgItem(IDC_LIST_FACE))->GetCurSel();

	if (i>=0)
	{
		int n=m_faces.size()/4;

		if (i<=(n-2))
		{
			for (int k=i;k<(n-1);k++)
			{
				m_faces[4*k]=m_faces[4*k+4];
				m_faces[4*k+1]=m_faces[4*k+5];
				m_faces[4*k+2]=m_faces[4*k+6];
				m_faces[4*k+3]=m_faces[4*k+7];

			


				allface[framenum].faces[4*k]=m_faces[4*k];
				allface[framenum].faces[4*k+1]=m_faces[4*k+1];
				allface[framenum].faces[4*k+2]=m_faces[4*k+2];
				allface[framenum].faces[4*k+3]=m_faces[4*k+3];
			}
		}
		m_faces.pop_back();
		m_faces.pop_back();
		m_faces.pop_back();
		m_faces.pop_back();

		allface[framenum].faces.pop_back();
		allface[framenum].faces.pop_back();
		allface[framenum].faces.pop_back();
		allface[framenum].faces.pop_back();

		((CListBox* )GetDlgItem(IDC_LIST_FACE))->DeleteString(i);
		GetImageDisplayData(m_dspbuf, m_image,  m_hpos,m_vpos);
		OnShowAllFaces();
	}
}


void CFaceAssistantDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	OnShowSigleFace();

	CDialogEx::OnRButtonUp(nFlags, point);
}
void CFaceAssistantDlg::SaveFaceInfoToTxt()
{
	if (m_currentFilePath.GetLength()<1)//第一次打开新文件，或者未打开文件就直接退出
	{
		return;
	}

	if (m_fileType==1)//图片
	{
		WriteFaceInfoToTxt(0);
	}
	else if (m_fileType==2)
	{
		int i=CurrentFrameNum;

		if (i>=0)
		{
			WriteFaceInfoToTxt(i);
		}
		
	}
}

void CFaceAssistantDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	SaveFaceInfoToTxt();
	SaveRecognitionInfo();

	CDialogEx::OnClose();
}


void CFaceAssistantDlg::OnDeleteAllFaces()
{
	// TODO: 在此添加命令处理程序代码

	if (!m_faces.empty())
	{
		m_faces.clear();
		vector<int>().swap(m_faces);
	}

	((CListBox* )GetDlgItem(IDC_LIST_FACE))->ResetContent();
	GetImageDisplayData(m_dspbuf, m_image,  m_hpos,m_vpos);

	Invalidate(FALSE);
}

void CFaceAssistantDlg::SelectFace(CPoint point)//选中一张人脸
{
	int n=m_faces.size()/4;
	int i;
	int minDistance=0;

	int x=point.x;
	int y=point.y-30;

	for (i=0;i<n;i++)
	{	
			
	}
}

void CFaceAssistantDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_image)
	{

		int t=m_image->width-m_dspbuf->width;

		if (t>0)
		{
			m_SBar.SetScrollRange(0,100);//水平滑动条范围设置为 0 到100
			//m_SBar.SetScrollPos(0); //水平滑动条初始位置设置为 0 
			int position=m_SBar.GetScrollPos();

			switch (nSBCode)
			{
			case SB_LINELEFT://向左滚动一行 
				position--; 
				break; 
			case SB_LINERIGHT://向右滚动一行 
				position++; 
				break; 
			case SB_PAGELEFT: //向左滚动一页 
				position-=10; 
				break; 
			case SB_PAGERIGHT: //向右滚动一页 
				position+=10; 
				break; 
			case SB_THUMBTRACK://拖动滑动块 
				position=nPos;//这里的nPos 就是函数 OnHScroll()的第 2 个参数 
				break; 
			case SB_LEFT://移动到最左边 
				position=0; 
				break; 
			case SB_RIGHT://移动到最右边 
				position=100; 
				break; 
			} 
			if(position>100) position=100; 
			if(position<0) position=0; 
			m_SBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置

			m_hpos=position*t/100;

			GetImageDisplayData(m_dspbuf, m_image, m_hpos,m_vpos );
//			DrawAllFace( m_dspbuf,false);
			ShowSigleFace();
			Invalidate(FALSE);

		} 
	} 
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CFaceAssistantDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_image)
	{
		int t=m_image->height-m_dspbuf->height;
		if (t>0)
		{
			m_SvBar.SetScrollRange(0,100);//水平滑动条范围设置为 0 到100
			//m_SBar.SetScrollPos(0); //水平滑动条初始位置设置为 0 
			int position=m_SvBar.GetScrollPos();

			switch (nSBCode)
			{
			case SB_LINELEFT://向左滚动一行 
				position--; 
				break; 
			case SB_LINERIGHT://向右滚动一行 
				position++; 
				break; 
			case SB_PAGELEFT: //向左滚动一页 
				position-=10; 
				break; 
			case SB_PAGERIGHT: //向右滚动一页 
				position+=10; 
				break; 
			case SB_THUMBTRACK://拖动滑动块 
				position=nPos;//这里的nPos 就是函数 OnHScroll()的第 2 个参数 
				break; 
			case SB_LEFT://移动到最左边 
				position=0; 
				break; 
			case SB_RIGHT://移动到最右边 
				position=100; 
				break; 

			} 
			if(position>100) position=100; 
			if(position<0) position=0; 
			m_SvBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
			//		UpdateData(FALSE);
			m_vpos=position*t/100;

			GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);
//			DrawAllFace( m_dspbuf,false);
			ShowSigleFace();
			Invalidate(FALSE);
		} 
	} 
	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}
void CFaceAssistantDlg::InitScroll()
{
	m_hpos=0;
	m_vpos=0;
	m_SvBar.SetScrollPos(0);
	m_SBar.SetScrollPos(0);
}
void CFaceAssistantDlg::ShowSigleFace()
{
	int i=((CListBox* )GetDlgItem(IDC_LIST_FACE))->GetCurSel();

	if (i>=0)
	{
		int l,r,t,b;

		DrawAllFace( m_dspbuf,false);

		l=m_faces[4*i]-m_hpos;
		r=m_faces[4*i+2]-m_hpos;
		t=m_faces[4*i+1]-m_vpos;
		b=m_faces[4*i+3]-m_vpos;

		myDrawrectangular( m_dspbuf,l,t,r,b,0,255,0);
		//		myDrawrectangular( m_dspbuf,m_faces[4*i],m_faces[4*i+1],m_faces[4*i+2],m_faces[4*i+3],0,255,0);

		int framenum=((CListBox* )GetDlgItem(IDC_LIST_NUM))->GetCurSel();

		
		if (i>=allface[framenum].name.size())
		{
			allface[framenum].name.push_back("");
		}
		GetDlgItem(IDC_EDIT1)->SetWindowText(allface[framenum].name[i]);


		Invalidate(FALSE);
	}
	else
	{
		OnShowAllFaces();
	}

}

void CFaceAssistantDlg::GetRecognitionInfo()//获取 识别信息
{
	CString txtPath=CurrentPath+"\\recognition.txt";
	CString AnalysisTime;
	GetAnalysisTime(AnalysisTime,txtPath);

	txtPath=CurrentPath+"\\Record\\"+AnalysisTime+".txt";

	int i,j,n,k;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());

	
	if (AnalysisTime=="#####")
	{
		return;
	}

	if (!allface.empty())
	{
		for (i=0;i<allface.size();i++)
		{
			allface[i].faces.clear();
			vector<int>().swap(allface[i].faces);
			allface[i].name.clear();
			vector<CString>().swap(allface[i].name);
		}
		allface.clear();
		vector<FACE>().swap(allface);
	}

	FACE f;

	CString tmp;
	int temp;

	FILE *File=NULL;
	fopen_s(&File,filePath,"rb");
	if (File)//打开成功
	{
		fread(&n,sizeof(int),1,File);//多少帧

		for (i=0;i<n;i++)
		{	
			fread(&k,sizeof(int),1,File);	
			if (!f.faces.empty())
			{
				f.faces.clear();
				vector<int>().swap(f.faces);
			}
			if (!f.name.empty())
			{
				f.name.clear();
				vector<CString>().swap(f.name);
			}
			if (k==0)
			{
				allface.push_back(f);
				continue;
			}


			for (j=0;j<k;j++)
			{
				fread(&temp,sizeof(int),1,File);	//.xmin
				f.faces.push_back(temp);
				fread(&temp,sizeof(int),1,File);//	.ymin
				f.faces.push_back(temp);
				fread(&temp,sizeof(int),1,File);//.xmax	
				f.faces.push_back(temp);
				fread(&temp,sizeof(int),1,File);	//.ymax
				f.faces.push_back(temp);

				ReadCStringR(tmp,File);
				f.name.push_back(tmp);
			}
			allface.push_back(f);
		}
		fclose(File);
	}

	File=NULL;
}
void CFaceAssistantDlg::SaveRecognitionInfo()//保存 识别信息
{
	CString txtPath=CurrentPath+"\\recognition.txt";
	CString AnalysisTime;
	GetAnalysisTime(AnalysisTime,txtPath);

	if (AnalysisTime=="#####")
	{
		return;
	}
	if (allface.size()<1)
	{
		return;
	}
	txtPath=CurrentPath+"\\Record\\"+AnalysisTime+".txt";
	int i,j,n,k;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());
	CString filepath,name;

	FILE *File=NULL;
	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		n= allface.size();
		fwrite(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			k=allface[i].faces.size()/4;
			fwrite(&k,sizeof(int),1,File);	
			if (k==0)
			{
				continue;
			}
			for (j=0;j<k;j++)
			{
				fwrite(&allface[i].faces[4*j],sizeof(int),1,File);	
				fwrite(&allface[i].faces[4*j+1],sizeof(int),1,File);	
				fwrite(&allface[i].faces[4*j+2],sizeof(int),1,File);	
				fwrite(&allface[i].faces[4*j+3],sizeof(int),1,File);
				
				WriteCStringR(allface[i].name[j],File);
			}

		}
		fclose(File);
	}

	File=NULL;
}
void CFaceAssistantDlg::changeName()
{
	// TODO: 在此添加控件通知处理程序代码
	CString name;
	GetDlgItemText(IDC_EDIT1,name);

	int i=((CListBox* )GetDlgItem(IDC_LIST_FACE))->GetCurSel();

	if (i>=0)
	{

		int framenum=((CListBox* )GetDlgItem(IDC_LIST_NUM))->GetCurSel();

		allface[framenum].name[i]=name;

		Invalidate(FALSE);
	}

	//更新画面和list
	((CListBox* )GetDlgItem(IDC_LIST_FACE))->DeleteString(i);
	((CListBox* )GetDlgItem(IDC_LIST_FACE))->InsertString(i,name);
	((CListBox* )GetDlgItem(IDC_LIST_FACE))->SetCurSel(i);
	OnShowSigleFace();
}
