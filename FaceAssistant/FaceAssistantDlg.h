
// FaceAssistantDlg.h : 头文件
//

#pragma once

#include "MyFunctions.h"
#include "afxwin.h"

typedef struct myData 
{
	vector<int> faces;
	vector<CString> name;
} FACE;

// CFaceAssistantDlg 对话框
class CFaceAssistantDlg : public CDialogEx
{
// 构造
public:
	CFaceAssistantDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_FACEASSISTANT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


	protected:

		CString CurrentPath;
		CString m_currentFilePath,m_currentFileName;
		int m_fileType;
		CString currentFrameFaceFilePath;//存储当前图片，或者当前帧人脸信息的文件
		CString m_AnalysisTime;
		vector<int> m_faces;//存储人脸信息
		vector<FACE> allface;//存储人脸识别信息

		IplImage* m_image, * m_dspbuf;
		CPoint m_point,leftUpPoint;
		bool addFaceFlag;
		int CurrentFrameNum;
		int m_hpos,m_vpos;

		void ReadAnalysisTime(vector<CString> &paths,CString txtPath);
		void GetAnalysisTime(CString &AnalysisTime,CString txtPath);
		void GetFaceInfoFromTxt(int TxtIndex);
		void WriteFaceInfoToTxt(int TxtIndex);
		void MyInit();//初始化设置
		void MySetWindow();
		void DrawAllFace( IplImage* src,bool f=true);
		BOOL PreTranslateMessage(MSG* pMsg);
		void SaveFaceInfoToTxt();
		void SelectFace(CPoint point);
		void InitScroll();
		void ShowSigleFace();
		void GetRecognitionInfo();//获取 识别信息
		void CFaceAssistantDlg::SaveRecognitionInfo();//保存 识别信息

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnOpenFile();
	afx_msg void OnShowSigleFace();
	afx_msg void OnShowSingleFrame();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnShowAllFaces();
	afx_msg void OnDeleteSelectedFace();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnClose();
	afx_msg void OnDeleteAllFaces();
	CScrollBar m_SBar;
	CScrollBar m_SvBar;
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void changeName();
};
