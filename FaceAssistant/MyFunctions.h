#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include "afxcmn.h"

#include <direct.h>
#include "shlwapi.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>
#include <iostream>
#include <time.h>

#include "windows.h"
#include <fstream>
#include <shlwapi.h>

using namespace std;

//==================================================================================
//===========                  ★★☆☆ IplImage 处理 ☆☆★★                    ======================================
//==================================================================================

void SetPointColour(IplImage* dst,int x,int y,int r,int g,int b);
void SetPointColour(IplImage* dst,int x,int y,IplImage* src,int p_x,int p_y);
void InitPictureColour(IplImage* image,int r,int g,int b);//初始化图片颜色值
void InitPictureColour(IplImage* image,int l,int r,int t,int b,int R,int G,int B);//初始化图片一个区域
void SetImageInCenter(IplImage* dst, IplImage* src,bool f);
void GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos);
void SetBackPic(IplImage* dst,CString pathname,int x,int y,int w,int h,int p_x,int p_y,bool f);//f==false原图，f==true镜像对称
void myLoadPicture(CString picturePath,IplImage* &image);
void myLoadPictureBy4channels(CString picturePath,IplImage*& image);
void myDrawrectangular(IplImage* image,int l,int t,int r,int b,BYTE R,BYTE G,BYTE B);
void MyCopyImage(IplImage* &dst, IplImage* src);
void MyCopyImage(IplImage* &dst, IplImage* src,int x,int y,int w,int h);
void MyCopyImage(IplImage* &dst,int x,int y, IplImage* src);
void SetDspbufPic(IplImage* &dst,CString pathname,int x,int y,int w,int h);
void CopyInHorizontal(IplImage* image,int l,int r,int t,int b,int x);//水平方向复制一段图片,横向复制范围包括两个端点，纵向不包括下边界
void CopyInVertical(IplImage* image,int l,int r,int t,int b,int y);
void CopyImageInHorizontalByMirror(IplImage* image,int l,int r,int t,int b,int x);//水平方向复制一段图片,镜面复制
void CopyImageInHorizontal(IplImage* image,int l,int r,int t,int b,int x);//水平方向复制一段图片,平移复制
void CopyImageInVerticalByMirror(IplImage* image,int l,int r,int t,int b,int y);//垂直方向复制一段图片,镜面复制
void RateOfAdvance(IplImage* image,int l,int r,int t,int b);//画一个进度条
void RateOfAdvance(IplImage* image,int l,int r,int t,int b,float rate);
void SetPictureInAPart(IplImage* image,int x,int y,int w,int h,CString pathname);//在一个指定区域显示缩略图
void GetPictureFromVideoByIndex(CString videoPath,IplImage*& image,int index);//根据帧序号，获取任意视频帧图片
int GetVideoFraneCounts(CString videoPath);//获取视频帧总数

//==================================================================================
//===========          ★★☆☆ Windows文件目录 处理 ☆☆★★        =========================================
//==================================================================================

void CreateNewDir(CString path);//新建一个目录
void WriteCStringR(CString s,FILE *fp);//自动换行
void ReadCStringR(CString &s,FILE *fp);
void WriteCString(CString s,FILE *fp);//将字符串写入文件
void ReadCString(CString &s,FILE *fp);//从文件中读取CString
void DeleteDir(CString path);//删除一个目录
BOOL DirectoryEmpty(CString path,CString format);//判断目录下面有没有.jpg格式文件，没有就是空
void MyDeleteFile(CString FilePath);//删除文件
void MyRename(CString OldName,CString NewName);//对文件重命名
void MyCopyFile(CString OldPath,CString NewPath);//复制文件
void MyMoveFile(CString OldPath,CString NewPath);//剪切文件


//==================================================================================
//===========        ★★☆☆ Windows系统相关 ☆☆★★        =============================================
//==================================================================================

void MyGetTime(CString &AnalysisTime);//获取当前时间字符串，14位
void MyGetTimeByAnalysisTime(CString AnalysisTime,CString &mytime);//将14位时间字符串 转换为 年月日字符串
void MyGetDeskTopPath(CString &DeskTopPath);//获取桌面路径
void MyCirculateSolution();//解决循环假死
void GetCurrentPath(CString &CurrentPath);//获取当前exe绝对路径


//==================================================================================
//============           ★★☆☆ CString 相关 ☆☆★★          =============================================
//==================================================================================

void ClearCStringVector(vector<CString> &data);
char* CStringToChar(CString data);// CString 转 char*
void GetFilenameFromAbsolutePath(CString &path,CString &filename);// 从绝对路径/相对路径 中获取文件名
void GetFileformat(CString &filepath,CString &format);//从路径/字符串中 获取文件格式
int IsPictureOrVideo(CString FilePath);//从路径/文件名中 判断是 常见的 图片格式 还是 视频格式