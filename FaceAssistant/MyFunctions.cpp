#include "stdafx.h"

#include "MyFunctions.h"



//==================================================================================
//===========                  ★★☆☆ IplImage 处理 ☆☆★★                    ======================================
//==================================================================================

void SetPointColour(IplImage* dst,int x,int y,int r,int g,int b)
{
	if (x<dst->width&&y<dst->height&&x>=0&&y>=0)
	{
		dst->imageData[ y* dst->widthStep + dst->nChannels * x + 2 ] =(BYTE)r;
		dst->imageData[ y* dst->widthStep + dst->nChannels * x + 1 ] =(BYTE)g;
		dst->imageData[ y* dst->widthStep + dst->nChannels *x  ] =(BYTE)b;
	}
	
}
void SetPointColour(IplImage* dst,int x,int y,IplImage* src,int p_x,int p_y)
{
	if (x<dst->width&&y<dst->height&&p_x<src->width&&p_y<src->height)
	{
		dst->imageData[ y* dst->widthStep + dst->nChannels * x + 2 ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x + 2 ];
		dst->imageData[ y* dst->widthStep + dst->nChannels * x + 1 ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x + 1 ];
		dst->imageData[ y* dst->widthStep + dst->nChannels *x  ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x  ];
	}

}
void InitPictureColour(IplImage* image,int r,int g,int b)//初始化图片颜色值
{
	int i,j;
	for(i = 0; i < image->height; i++)
		for(j = 0; j < image->width; j++)
		{
			SetPointColour(image,j ,i,r,g,b);
		}
}
void InitPictureColour(IplImage* image,int l,int r,int t,int b,int R,int G,int B)//初始化图片一个区域
{
	int i,j;
	for(i = t; i <b; i++)
		for(j = l; j <r; j++)
		{
			SetPointColour(image,j ,i,R,G,B);
		}
}
void SetImageInCenter(IplImage* dst, IplImage* src,bool f)
{
	int width,height;
	width=src->width;
	height=src->height;

	double rate=((double)dst->width)/width;
	if (rate<1)
	{
		width=dst->width;
		height=src->height*rate;
	}

	rate=((double)dst->height)/height;
	if (rate<1)
	{
		width=width*rate;
		height=dst->height;
	}
	if (f==true)
	{
		InitPictureColour(dst,0,dst->width,0,dst->height,215,215,215);//初始化背景色
	}

	rate=((double)src->width)/width;
	int x,y;
	x=(dst->width-width)/2.0;
	y=(dst->height-height)/2.0;

	int i,j;
	int i1,j1;

	for (i=x;i<(x+width);i++)
	{
		for (j=y;j<(y+height);j++)
		{
			i1=rate*(i-x);
			j1=rate*(j-y);
			SetPointColour(dst,i,j,src,i1,j1);
		}
	}
}
void GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos)
{
	int i,j,i2,j2;
	int linebyte = dspbuf->widthStep;
	int linebytesclae = imagescale->widthStep;
	char *temp_dsp = dspbuf->imageData, *temp_scale = imagescale->imageData;
	UINT *dsp = (UINT *)temp_dsp, *scale = (UINT *)temp_scale;

	if( imagescale->width <= dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width <= dspbuf->width && imagescale->height > dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = 0;
		for( i = 0; i < dspbuf->height; i++)
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = 0, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = 0; j < dspbuf->width; j++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height > dspbuf->height )
	{
		DWORD oldtime = GetTickCount();
		for( i = 0; i < dspbuf->height; i++)
			for( j = 0; j < dspbuf->width; j++ )
			{
				if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j+hpos)];
				}
			}
			DWORD time = GetTickCount() - oldtime;
			int kkk = 0;
	}
	temp_dsp = NULL, temp_scale = NULL;
	dsp = NULL,scale = NULL;
}

void SetBackPic(IplImage* dst,CString pathname,int x,int y,int w,int h,int p_x,int p_y,bool f)//f==false原图，f==true镜像对称
{
	char *path = (LPSTR)(LPCTSTR)pathname;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	int i,j,i1,j1,i2,j2;

	if (f==false)
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x+j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(dst,j1,i1,temp,j2,i2);
			}
	}
	else if (f==true)//右边镜像对称
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x-j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(dst,j1,i1,temp,j2,i2);
			}
	}
	cvReleaseImage(&temp);
}


void myLoadPicture(CString picturePath,IplImage* &image)
{
	char *path = (LPSTR)(LPCTSTR)picturePath;
	image = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!image)
	{
		image=NULL;
		return;
	}
}

void myLoadPictureBy4channels(CString picturePath,IplImage*& image)
{
	int i,j;
	char *path = (LPSTR)(LPCTSTR)picturePath;
	IplImage* temp =NULL;

	temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	cvReleaseImage(&image);
	image=cvCreateImage(cvSize(temp->width,temp->height),8,4);
	for(i =0; i <temp->height; i++)
		for(j =0; j <temp->width; j++)
		{
			SetPointColour(image,j,i,temp,j,i);
		}

		cvReleaseImage(&temp);

}
void myDrawrectangular(IplImage* image,int l,int t,int r,int b,BYTE R,BYTE G,BYTE B)
{
	int i,j;
	for (i=l;i<r;i++)
	{
		for (j=t;j<b;j++)
		{
			if (abs(i-l)<3||abs(r-i)<3||abs(j-t)<3||abs(j-b)<3)
			{
				SetPointColour(image,i,j,R,G,B);
			}
		}
	}
}
void MyCopyImage(IplImage* &dst, IplImage* src)
{
	int i,j;

	cvReleaseImage(&dst);
	dst=cvCreateImage(cvSize(src->width,src->height),src->depth,src->nChannels);
	for(i =0; i <src->height; i++)
		for(j =0; j <src->width; j++)
		{
			SetPointColour(dst,j,i,src,j,i);
		}
}
void MyCopyImage(IplImage* &dst, IplImage* src,int x,int y,int w,int h)
{
	int i,j;

	cvReleaseImage(&dst);
	dst=cvCreateImage(cvSize(w,h),src->depth,src->nChannels);
	for(i =0; i <h; i++)
		for(j =0; j <w; j++)
		{
			SetPointColour(dst,j,i,src,x+j,y+i);
		}
}
void MyCopyImage(IplImage* &dst,int x,int y, IplImage* src)
{
	int i,j;

	for(i =0; i <src->height; i++)
		for(j =0; j <src->width; j++)
		{
			SetPointColour(dst,x+j,y+i,src,j,i);
		}
}
void SetDspbufPic(IplImage* &dst,CString pathname,int x,int y,int w,int h)
{
	char *path = (LPSTR)(LPCTSTR)pathname;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}
	IplImage* tem_image=cvCreateImage(cvSize(w,h),temp->depth,temp->nChannels);
	cvResize( temp, tem_image, CV_INTER_NN  );
	int i,j,i1,j1,i2,j2;

	for(i =0; i <h; i++)
		for(j =0; j < w; j++)
		{
			i1=y+i;
			j1=x+j;
			i2=i;
			j2=j;
			SetPointColour(dst,j1,i1,tem_image,j2,i2);
		}

		cvReleaseImage(&temp);
		cvReleaseImage(&tem_image);
}
void CopyInHorizontal(IplImage* image,int l,int r,int t,int b,int x)
{
	int i,j;

	for (i=l;i<=r;i++)
	{
		for (j=t;j<b;j++)
		{
			SetPointColour(image,i,j,image,x,j);
		}
	}
}
void CopyImageInVerticalByMirror(IplImage* image,int l,int r,int t,int b,int y)
{
	int i,j;

	for (i=l;i<r;i++)
	{
		for (j=0;j<=(b-t);j++)
		{
			SetPointColour(image,i,(y+j),image,i,(b-j));
		}
	}
}
void CopyInVertical(IplImage* image,int l,int r,int t,int b,int y)
{
	int i,j;

	for (i=l;i<=r;i++)
	{
		for (j=t;j<=b;j++)
		{
			SetPointColour(image,i,j,image,i,y);
		}
	}
}
void CopyImageInHorizontalByMirror(IplImage* image,int l,int r,int t,int b,int x)//水平方向复制一段图片,镜面复制
{
	int i,j;

	for (i=0;i<=(r-l);i++)
	{
		for (j=t;j<b;j++)
		{
			SetPointColour(image,(x+i),j,image,(r-i),j);
		}
	}
}
void CopyImageInHorizontal(IplImage* image,int l,int r,int t,int b,int x)//水平方向复制一段图片,平移复制
{
	int i,j;

	for (i=0;i<=(r-l);i++)
	{
		for (j=t;j<b;j++)
		{
			SetPointColour(image,(x+i),j,image,(l+i),j);
		}
	}
}
void RateOfAdvance(IplImage* image,int l,int r,int t,int b)//画一个进度条
{
	int i,j;
	for (i=l;i<=r;i++)
	{
		for (j=t;j<=b;j++)
		{
			if (i==l||i==r||j==t||j==b)
			{
				SetPointColour(image,i ,j,0,0,0);
			}
		}
	}
}
void RateOfAdvance(IplImage* image,int l,int r,int t,int b,float rate)//画一个进度条
{
	int i,j;
	int k=(int)((r-l)*rate);
	int g;
	for (i=l+1;i<(l+k);i++)
	{
		g=180;
		for (j=t+1;j<b;j++)
		{
			if (j<=(t+b)/2)
			{
				g=g+5;
			}
			else
			{
				g=g-5;
			}
			SetPointColour(image,i ,j,0,g,0);
		}
	}
}


void SetPictureInAPart(IplImage* image,int x,int y,int w,int h,CString pathname)//在一个指定区域显示缩略图
{
	char *path = (LPSTR)(LPCTSTR)pathname;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	x=x+w/2;
	y=y+h/2;
	w=w-6;
	h=h-6;

	double rate;
	int tmp_w=temp->width;
	int tmp_h=temp->height;
	int middle_x=tmp_w/2;
	int middle_y=tmp_h/2;

	//默认长宽都小于显示区域
	rate=(double)(temp->height)/h;

	if (tmp_h>h)
	{
		rate=(double)(temp->height)/h;
		tmp_h=h;
		tmp_w=(int)(temp->width/rate);
	}
	if (tmp_w>w)
	{
		rate=(double)(temp->width)/w;
		tmp_h=(int)(temp->height/rate);
		tmp_w=w;
	}


	if (rate<1)
	{
		rate=1;
	}

	int i,j,i1,j1,i2,j2;
	for(i =0; i <tmp_h; i++)
		for(j =0; j <tmp_w; j++)
		{
			i1=y-tmp_h/2+i;
			j1=x-tmp_w/2+j;
			i2=(int)(rate*i);
			j2=(int)(rate*j);
			SetPointColour(image,j1,i1,temp,j2,i2);
		}
		cvReleaseImage(&temp);
}
void GetPictureFromVideoByIndex(CString videoPath,IplImage*& image,int index)
{
	int i,j;
	char *path = (LPSTR)(LPCTSTR)videoPath;
	IplImage* temp =NULL;

	cvReleaseImage(&image);

	CvCapture* pCapture = cvCaptureFromFile(path);
	if( !pCapture)
	{
		image=NULL;
		return ;
	}

	cvSetCaptureProperty(pCapture, CV_CAP_PROP_POS_FRAMES, index);
	temp = cvQueryFrame( pCapture );
	if (!temp)
	{
		image=NULL;
		return;
	}

	image=cvCreateImage(cvSize(temp->width,temp->height),8,4);
	for(i =0; i <temp->height; i++)
		for(j =0; j <temp->width; j++)
		{
			SetPointColour(image,j,i,temp,j,i);
		}

	cvReleaseCapture(&pCapture);
}
int GetVideoFraneCounts(CString videoPath)
{
	int i,j;
	char *path = (LPSTR)(LPCTSTR)videoPath;

	CvCapture* pCapture = cvCaptureFromFile(path);
	if( !pCapture)
	{
		return 0;
	}
	int vframes = cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_COUNT);
	return vframes;
}

//==================================================================================
//===========          ★★☆☆ Windows文件目录 处理 ☆☆★★        =========================================
//==================================================================================

void CreateNewDir(CString path)//新建一个目录
{
	if(!PathFileExists(path))
	{
		_mkdir(path);
	}
}
void WriteCStringR(CString s,FILE *fp)//自动换行
{
	char *a;
	int j=0;
	int k;//存储着数组长度
	s=s+"\r\n";
	a=s.GetBuffer(s.GetLength());
	k=(int)strlen(a);
	char c;
	for (j=0;j<k;j++)
	{
		c=a[j];
		fwrite(&a[j],sizeof(char),1,fp);
	}
}
void ReadCStringR(CString &s,FILE *fp)
{
	char p[1000];
	fgets(p,1000,fp);
	CString str;
	str.Format(_T("%s"), p);
	s=str.Left(str.GetLength()-2);//去除 \r\n
}
void WriteCString(CString s,FILE *fp)
{
	char *a;
	int j=0;
	int k;//存储着数组长度

	s=s+"\n";//读取时每次读一行
	a=s.GetBuffer(s.GetLength());
	k=(int)strlen(a);
	char c;
	for (j=0;j<k;j++)
	{
		///////////////////////加密/////////////////////////////
		if (j<(k-1))//加密 最后换行符不加密
		{
			c=a[k-2-j]-7;//加密
		}
		else
		{
			c=a[j];
		}

		fwrite(&c,sizeof(char),1,fp);
		////////////////////////////////////////////////////////

		// 		c=a[j];
		// 		fwrite(&a[j],sizeof(char),1,fp);

	}
}
void ReadCString(CString &s,FILE *fp)
{
	char p[1000];
	fgets(p,1000,fp);
	CString str;
	str.Format(_T("%s"), p);
	s=str.Left(str.GetLength()-1);//为加密情况下直接读取
	///////////////////////////////////////////////////解密处理
	s="";
	char c;
	int k=(int)strlen(p);
	int i;
	for (i=0;i<k;i++)
	{
		if (i<(k-1))
		{
			c=p[k-2-i]+7;
			s+=c;
		}

	}
	//////////////////////////////////////////////
	//	int k=0;
}

void DeleteDir(CString path)//删除一个目录
{
	if(PathFileExists(path))
	{
		_rmdir(path);//删除一个目录，若成功则返回0，否则返回-1
	}
}
BOOL DirectoryEmpty(CString path,CString format)//判断目录下面有没有.jpg格式文件，没有就是空
{
	CString tmp=path+"\\*."+format;

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(tmp, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
	{
		return TRUE;//为空
	}
	else 
	{
		return FALSE;//不为空
	}
}
void MyDeleteFile(CString FilePath)//删除文件
{
	if (PathFileExists(FilePath))
	{		
		TCHAR* filename =FilePath.GetBuffer(FilePath.GetLength()); 
		DeleteFile(filename);
	}
}
void MyRename(CString OldName,CString NewName)//对文件重命名
{
	TCHAR* pOldName =OldName.GetBuffer(OldName.GetLength());; 
	TCHAR* pNewName =NewName.GetBuffer(NewName.GetLength()); 

	CFile::Rename(pOldName, pNewName);
}
void MyCopyFile(CString OldPath,CString NewPath)//复制文件
{
		CopyFile(OldPath,NewPath,TRUE);
}
void MyMoveFile(CString OldPath,CString NewPath)//剪切文件
{
	MoveFile(OldPath,NewPath);
}

//==================================================================================
//===========        ★★☆☆ Windows系统相关 ☆☆★★        =============================================
//==================================================================================

void MyGetTime(CString &AnalysisTime)
{
	SYSTEMTIME tmp_time;
	GetLocalTime(&tmp_time);
	AnalysisTime.Format("%4d%02d%02d%02d%02d%02d",tmp_time.wYear,tmp_time.wMonth,tmp_time.wDay,tmp_time.wHour,tmp_time.wMinute,tmp_time.wSecond);
}
void MyGetTimeByAnalysisTime(CString AnalysisTime,CString &mytime)
{
	CString tmp;
	tmp=AnalysisTime.Left(4)+"年";
	mytime=tmp;
	tmp=AnalysisTime.Right(10);
	tmp=tmp.Left(2)+"月";
	mytime=mytime+tmp;
	tmp=AnalysisTime.Right(8);
	tmp=tmp.Left(2)+"日";
	mytime=mytime+tmp;
}

void MyGetDeskTopPath(CString &DeskTopPath)//获取桌面路径
{
	TCHAR DeskToppath[255];
	SHGetSpecialFolderPath(0,DeskToppath,CSIDL_DESKTOPDIRECTORY,0);//获取桌面路径

	DeskTopPath.Format(_T("%s"),DeskToppath);
}
void MyCirculateSolution()//解决循环假死
{
	MSG msg;
	while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		::TranslateMessage(&msg);
		::DispatchMessage(&msg);
	}
}
void GetCurrentPath(CString &CurrentPath)//获取当前exe绝对路径
{
	CString path; 
	GetModuleFileName(NULL,path.GetBufferSetLength(MAX_PATH+1),MAX_PATH); 
	path.ReleaseBuffer(); 
	int pos = path.ReverseFind('\\'); 
	CurrentPath = path.Left(pos);
}


//==================================================================================
//============           ★★☆☆ CString 相关 ☆☆★★          =============================================
//==================================================================================

void ClearCStringVector(vector<CString> &data)
{
	if (!data.empty())
	{
		data.clear();
		vector<CString>().swap(data);
	}
}
char* CStringToChar(CString data)
{
	return data.GetBuffer(data.GetLength());
}
void GetFilenameFromAbsolutePath(CString &path,CString &filename)
{
	int pos = path.ReverseFind('\\'); 
	filename = path.Right(path.GetLength()-pos-1);
}
void GetFileformat(CString &filepath,CString &format)
{
	int pos = filepath.ReverseFind('.'); 
	format = filepath.Right(filepath.GetLength()-pos-1);
}
int IsPictureOrVideo(CString FilePath)
{
	if (FilePath=="")
	{
		return 0;
	}

	CString picture[8]={"jpg","png","jpeg","bmp","JPG","PNG","JPEG","BMP"};
	CString video[10]={"mp4","avi","wmv","rm","rmvb","mov","flv","mkv","vob","mpeg"};

	CString format; 
	GetFileformat(FilePath,format);

	int i;

	for (i=0;i<8;i++)
	{
		if (format==picture[i])
		{
			return 1;//图片文件
		}
	}

	for (i=0;i<10;i++)
	{
		if (format==video[i])
		{
			return 2;//视频文件
		}
	}

	return 0;//其他文件
}